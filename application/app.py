#!flask/bin/python
from flask import Flask
from flask import jsonify
from flask import request
from flask import abort

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.ext.flask.middleware import XRayMiddleware

import logging
import sys

app = Flask(__name__)
app.url_map.strict_slashes = False

xray_recorder.configure(service='details')
XRayMiddleware(app, xray_recorder)


@app.route('/')
def index():

    return "Hello, World!"


@app.route('/v1/details/<book_id>')
def details_v1(book_id):

    try:
        book_id = int(book_id)
    except ValueError:
        abort(400, 'Please provide valid integer')

    headers = get_request_headers(request.headers)

    details = get_book_details(book_id, headers)

    return jsonify(details)


@app.route('/v2/details/<book_id>')
def details_v2(book_id):

    try:
        book_id = int(book_id)
    except ValueError:
        abort(400, 'Please provide valid integer')

    headers = get_request_headers(request.headers)

    details = get_book_details(book_id, headers)

    return jsonify(details)


@app.route('/health')
def health():

    resp = {
        'status': 'Details app is healthy'
    }

    return jsonify(resp)


def get_book_details(book_id, headers):

    return {
        'id': book_id,
        'author': 'William Shakespeare',
        'year': '1595',
        'type': 'paperback',
        'pages': 200,
        'publisher': 'PublisherA',
        'language': 'English',
        'ISBN-10': '1234567890',
        'ISBN-13': '123-1234567890'
    }


def get_request_headers(request_headers):

    headers = {}
    incoming_headers = ['x-request-id',
                        'x-b3-traceid',
                        'x-b3-spanid',
                        'x-b3-parentspanid',
                        'x-b3-sampled',
                        'x-b3-flags',
                        'x-ot-span-context',
                        'x-datadog-trace-id',
                        'x-datadog-parent-id',
                        'x-datadog-sampled'
                        ]

    for header, value in request_headers:
        if header in incoming_headers:
            headers[header] = value

    return headers


if __name__ == '__main__':
    if len(sys.argv) < 2:
        logging.error("usage: %s port" % (sys.argv[0]))
        sys.exit(-1)

    p = int(sys.argv[1])
    logging.info("start at port %s" % (p))
    # Python does not work on an IPv6 only host
    # https://bugs.python.org/issue10414
    app.run(host='0.0.0.0', port=p, debug=True, threaded=True)
