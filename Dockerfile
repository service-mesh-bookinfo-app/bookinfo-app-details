FROM python:3.7.7-slim

COPY application/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY application/app.py /opt/microservices/
COPY application/requirements.txt /opt/microservices/

EXPOSE 9080
WORKDIR /opt/microservices

CMD ["python", "app.py", "9080"]